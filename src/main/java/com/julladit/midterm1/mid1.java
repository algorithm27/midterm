/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm1;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class mid1 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = kb.nextInt();
        }
        System.out.println("Maximum Subarray sum of A is " + maxSubArraySum(arr));
    }

    public static int maxSubArraySum(int[] arr) {
        int len = arr.length;
        int max = Integer.MIN_VALUE, end = 0;

        for (int i = 0; i < arr.length - 1; i++) {
            int sum = 0;
            for (int j = i; j <= arr.length - 1; j++) {
                sum += arr[j];
                if (sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }
}
